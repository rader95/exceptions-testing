public class ExceptionsTesting {
    public static void main(String[] args) {
        int[] tab = new int[] {1,4,7,3};
        //System.out.println(tab[2]);
        try {
            System.out.println(tab[5]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("Out of array range");
        } finally {
            System.out.println(tab[1]);
        }
        System.out.println(tab[2]);
    }
}
